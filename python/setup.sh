#!/bin/sh

PACKAGE_DIR="package"
SVG_DIR="svg"
NUMPY_DIR="numpy"

rm -rf package
mkdir package
cd package

if [ ! -d SVG_DIR ]; then
    pip install svg.charts --target .
fi

if [ ! -d NUMPY_DIR ]; then
    pip install numpy --target .
fi

cd ..
zip -r lambda.zip .
