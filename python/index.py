import os
import sys
import uuid
import json

from pprint import pprint

sys.path.append('package/')
from svg.charts import line
import numpy as np

def transform(logs):
    arr = np.array(list(map(lambda x: [dict(name='UnfilteredBrake', value=x['UnfilteredBrake']), dict(name='Rpm', value=x['Rpm']), dict(name='Speed', value=x['Speed']), dict(name='Steering', value=x['Steering']), dict(name='Throttle', value=x['Throttle'])], logs['content'])))
    return arr.transpose().tolist()

def save_svg_to_file(transformed_log):
    name = transformed_log[0]['name']
    plots = list(map(lambda x: x['value'], transformed_log))
    chart = line.Line()
    fileds = ['']*len(plots)
    options = dict(
        scale_integers=True,
        area_fill=True,
        width=640,
        height=480,
        fields=fileds,
        graph_title='Log of Project Cars2',
        show_graph_title=True,
        no_css=False,
    )
    chart.__dict__.update(options)
    chart.add_data({'data': plots, 'title': name})
    file_name = '/tmp/{}.svg'.format(str(uuid.uuid4()))
    with open(file_name, mode='w') as f:
        f.write(chart.burn())

    pprint(file_name)
    return file_name

logs = ''
with open('../sample_json/telemetry.json') as f:
    logs = json.load(f)

transformed_logs = transform(logs)
pprint(transformed_logs)
file_names = []
for transform_log in transformed_logs:
    file_name = save_svg_to_file(transform_log)
    pprint(file_name)
    file_names.append(file_name)
pprint(file_names)

