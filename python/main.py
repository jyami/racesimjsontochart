import os
import sys
import uuid
import json
from pprint import pprint, pformat

import boto3

sys.path.append('package/')
from svg.charts import line
import numpy as np

s3_client = boto3.client('s3')

def get_logs_from_file(file_name):
    with open(file_name) as f:
        return json.load(f)

def transform(logs):
    arr = np.array(list(map(lambda x: [dict(name='UnfilteredBrake', value=x['UnfilteredBrake']), dict(name='Rpm', value=x['Rpm']), dict(name='Speed', value=x['Speed']), dict(name='Steering', value=x['Steering']), dict(name='Throttle', value=x['Throttle'])], logs['content'])))
    return arr.transpose().tolist()

def save_svg_to_file(transformed_log):
    name = transformed_log[0]['name']
    plots = list(map(lambda x:x['value'], transformed_log))
    chart = line.Line()
    options = dict(
        scale_integers=True,
        area_fill=True,
        width=640,
        height=480,
        fields=['']*len(plots),
        graph_title='Log of Project Cars2',
        show_graph_title=True,
        no_css=False,
    )
    chart.__dict__.update(options)
    chart.add_data({'data': plots, 'title': name})
    file_name = '/tmp/{}.svg'.format(str(uuid.uuid4()))
    with open(file_name, mode='w') as f:
        f.write(chart.burn())
    return file_name

def handler(event, context):
    for record in event['Records']:
        src_bucket = record['s3']['bucket']['name']
        src_key = record['s3']['object']['key']
        download_path = '/tmp/{}{}'.format(uuid.uuid4(), src_key)
        
        s3_client.download_file(src_bucket, src_key, download_path)
        
        logs = get_logs_from_file(download_path)
        transformed_logs = transform(logs)
        file_names = map(save_svg_to_file, transformed_logs)
        for file_name in file_names:
            dest_bucket = src_bucket
            dest_key = file_name
            s3_client.upload_file(file_name, dest_bucket, dest_key[5:])
            
