package main

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

func lambdaMain() {
	lambda.Start(handler)
}

type MyResponse struct {
	Message string `json:"Answer:"`
}

func handler(ctx context.Context, s3Event events.S3Event) (MyResponse, error) {
	//readBucket := os.Getenv("READ_BUCKET")
	//writeBucket := os.Getenv("WRITE_BUCKET")
	writeBucket := "simracesvg"

	var r string
	for _, record := range s3Event.Records {
		s3 := record.S3
		r = fmt.Sprintf("[%s - %s- %s] Bucket = %s, Key = %s \n", record.EventSource, record.EventName, record.EventTime, s3.Bucket.Name, s3.Object.Key)
		fmt.Printf(r)
		readBucket := s3.Bucket.Name
		readKey := s3.Object.Key
		telemetryChart(readBucket, readKey, writeBucket)
	}
	return MyResponse{Message: r}, nil
}

func telemetryChart(readBucket string, readKey string, writeBucket string) {
	fmt.Printf(fmt.Sprintf("readBucket = %s, readKey = %s, writeBucket = %s\n", readBucket, readKey, writeBucket))
	s3 := s3NewService()
	//_, err := fileIsExistOnS3(s3, readBucket, readKey)
	//if err != nil {
	//	panic(err)
	//}

	r, err := s3GetObject(s3, readBucket, readKey)
	if err != nil {
		panic(err)
	}
	if r != nil {
		defer r.Close()
	}

	bfSVG, err := drawSVGChart(r)
	if err != nil {
		panic(err)
	}

	err = s3PutObject(s3, bfSVG, writeBucket, readKey+".svg")
	if err != nil {
		panic(err)
	}
}

func s3NewService() *s3.S3 {
	svc := s3.New(session.New(), &aws.Config{
		Region: aws.String(endpoints.UsEast1RegionID),
	})
	return svc
}

func awsError(err error) error {
	fmt.Printf("awsError")
	if aerr, ok := err.(awserr.Error); ok {
		return fmt.Errorf("aws error %v", aerr.Error())
	}
	return fmt.Errorf("error %v", err.Error())
}

func fileIsExistOnS3(svc *s3.S3, bucket string, key string) (bool, error) {
	fmt.Printf("fileIsExistOnS3\n")
	// ファイルの存在確認
	loo, errlo := svc.ListObjectsV2(&s3.ListObjectsV2Input{
		Bucket: aws.String(bucket),
		Prefix: aws.String(key),
	})

	if errlo != nil {
		return false, awsError(errlo)
	}
	return *loo.KeyCount >= 1, nil
}

func s3GetObject(svc *s3.S3, bucket string, key string) (io.ReadCloser, error) {
	fmt.Printf("s3GetObject\n")
	goo, errgo := svc.GetObject(&s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	})
	if errgo != nil {
		fmt.Printf("errgo != nil")
		return nil, awsError(errgo)
	}

	return goo.Body, errgo
}

func s3PutObject(svc *s3.S3, r io.Reader, bucket string, key string) error {
	bs, err := ioutil.ReadAll(r)

	_, err = svc.PutObject(&s3.PutObjectInput{
		Body:   bytes.NewReader(bs),
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
		//ACL:                  aws.String("private"),
		//ServerSideEncryption: aws.String("AES256"),
	})

	fmt.Printf("PutObject end")

	if err != nil {
		fmt.Printf("err != nil")
		return awsError(err)
	}

	return nil
}
