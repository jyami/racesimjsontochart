package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"

	"github.com/wcharczuk/go-chart"
)

type TelemetryFormat struct {
	UnfilteredThrottle uint8   `json:"UnfilteredThrottle"` //unsigned char							sUnfilteredThrottle;							// 13 1
	UnfilteredBrake    uint8   `json:"UnfilteredBrake"`    //unsigned char							sUnfilteredBrake;									// 14 1
	UnfilteredSteering int8    `json:"UnfilteredSteering"` //signed char								sUnfilteredSteering;							// 15 1
	Brake              uint8   `json:"Brake"`              //unsigned char							sBrake;														// 29 1
	Throttle           uint8   `json:"Throttle"`           //unsigned char							sThrottle;												// 30 1
	FuelLevel          float32 `json:"FuelLevel"`          //float									sFuelLevel;												// 32 4
	Speed              float32 `json:"Speed"`              //float									sSpeed;														// 36 4
	Rpm                uint16  `json:"Rpm"`                //unsigned short							sRpm;															// 40 2
	Steering           int8    `json:"Steering"`           //signed char								sSteering;												// 44 1
	GearNumGears       uint8   `json:"GearNumGears"`       //unsigned char							sGearNumGears;										// 45 1
	EventTimeRemaining float32 `json:"EventTimeRemaining"` //float							sEventTimeRemaining;									// 17  // time remaining, -1 for invalid time,  -1 - laps remaining in lap based races  --
	Sector             uint8   `json:"Sector"`             //unsigned char							sSector;										// 15 -- sector + extra precision bits for x/z position
	CurrentSectorTime  float32 `json:"CurrentSectorTime"`  //float									sCurrentSectorTime;								// 26 --
	CurrentLapDistance uint16  `json:"CurrentLapDistance"` //unsigned short							sCurrentLapDistance;							// 12 --
	CurrentLap         uint8   `json:"CurrentLap"`         //unsigned char							sCurrentLap;									// 21 --
	TickCount          uint32  `json:"TickCount"`          // 555
}

func jsonToSeries(r io.Reader) ([]chart.Series, error) {
	fmt.Println("jsonToSeries")
	s := bufio.NewScanner(r)
	line := 0
	var throttleY []float64
	var brakeY []float64
	var speedY []float64
	var x []float64
	for s.Scan() {
		line++
		//fmt.Printf("%d\n", line)
		x = append(x, float64(line))

		var t TelemetryFormat
		json.Unmarshal(s.Bytes(), &t)

		throttleY = append(throttleY, float64(t.Throttle))
		//fmt.Printf("brake: %d\n", t.Brake)
		brakeY = append(brakeY, float64(t.Brake))
		//fmt.Printf("speed: %f\n", t.Speed)
		speedY = append(speedY, float64(t.Speed))
	}
	//fmt.Printf("brakeY: %v\n", brakeY)
	throttle := chart.ContinuousSeries{
		Name:    "Speed",
		Style:   chart.StyleShow(),
		XValues: x,
		YValues: throttleY,
	}
	brake := chart.ContinuousSeries{
		Name:    "Brake",
		Style:   chart.StyleShow(),
		XValues: x,
		YValues: brakeY,
	}
	speed := chart.ContinuousSeries{
		Name:    "Speed",
		Style:   chart.StyleShow(),
		XValues: x,
		YValues: speedY,
	}

	v := []chart.Series{
		throttle,
		brake,
		speed,
	}

	return v, nil
}

func createGraph(series []chart.Series) (chart.Chart, error) {
	fmt.Println("createGraph")
	graph := chart.Chart{

		XAxis: chart.XAxis{
			Name:           "The XAxis",
			NameStyle:      chart.StyleShow(),
			Style:          chart.StyleShow(),
			ValueFormatter: chart.TimeMinuteValueFormatter, //TimeHourValueFormatter,
		},

		YAxis: chart.YAxis{
			Name:      "The YAxis",
			NameStyle: chart.StyleShow(),
			Style:     chart.StyleShow(),
		},

		//YAxisSecondary: chart.YAxis{
		//	Name:      "The YAxis secondary",
		//	NameStyle: chart.StyleShow(),
		//	Style:     chart.StyleShow(),
		//},

		Series: series,
	}

	return graph, nil
}

func drawSVGChart(r io.Reader) (*bytes.Buffer, error) {

	series, err := jsonToSeries(r)
	if err != nil {
		panic(err)
	}

	graph, err := createGraph(series)
	if err != nil {
		panic(err)
	}

	bfSVG := bytes.NewBuffer([]byte{})

	err = graph.Render(chart.SVG, bfSVG)
	if err != nil {
		panic(err)
	}

	return bfSVG, nil
}
