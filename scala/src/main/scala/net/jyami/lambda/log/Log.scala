package net.jyami.lambda.log

import org.json4s._
import org.json4s.native.JsonMethods._

case class LogSeries(content: List[Log])
case class Log(Brake: Double, Throttle: Double, Speed: Double, Rpm: Double, Steering: Double)

object LogSeries {

  def fromJson(json: String): LogSeries = {
    implicit val formats = DefaultFormats
    val parsed = parse(json)
    parsed.extract[LogSeries]
  }
}