package net.jyami.lambda.aws

import java.io.File
import java.net.URLDecoder

import scala.collection.JavaConverters._
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.lambda.runtime.events.S3Event
import com.amazonaws.services.s3.model.{GetObjectRequest, ObjectMetadata, PutObjectRequest, S3Object}

object AwsS3 {
  val tokyoRegion = "ap-northeast-1"
  val s3Client = AmazonS3ClientBuilder.standard()
    .withRegion(tokyoRegion)
    .build

  def decodeS3Key(key: String): String = URLDecoder.decode(key.replace("+"," "), "UTF-8")

  def getSource(s3Event: S3Event): Seq[(String, String)] = {
    s3Event.getRecords.asScala.map { record =>
      val srcBucket = record.getS3.getBucket.getName
      val srcKey = decodeS3Key(record.getS3.getObject.getKey)
      (srcBucket, srcKey)
    }
  }

  def getObject(srcBucket: String, srcKey: String ): S3Object = {
    s3Client.getObject(new GetObjectRequest(srcBucket, srcKey))
  }

  def getTextFromStream(s3object: S3Object): String = {
    val input = s3object.getObjectContent
    scala.io.Source.fromInputStream(input).mkString
  }

  def putObject(bucket: String, key: String, file: File) = {
    val request = new PutObjectRequest(bucket, key, file)
    val metadata = new ObjectMetadata
    metadata.setContentType("image/svg+xml")
    request.setMetadata(metadata)
    s3Client.putObject(request)
  }
}
