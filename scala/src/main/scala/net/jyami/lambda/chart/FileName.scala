package net.jyami.lambda.chart

object FileName {
  val dir = "/tmp/"
  val UUID_LENGTH = 36

  def generate(): String = {
    val uuid = java.util.UUID.randomUUID.toString
    val fullPath = "%s%s.svg".format(dir, uuid)
    fullPath
  }

  def toKey(fileName: String): String = {
    fileName.substring(dir.length, dir.length + UUID_LENGTH)
  }
}
