package net.jyami.lambda.chart

import net.jyami.lambda.log.LogSeries
import org.knowm.xchart.VectorGraphicsEncoder.VectorGraphicsFormat
import org.knowm.xchart.{QuickChart, VectorGraphicsEncoder}

case class Series(title: String, points: Seq[Double])

object ChartSource {
  def fromJson(json: String): Seq[Series] = {
    val logSeries = LogSeries.fromJson(json)
    logSeries.content
      .map(l => List(("Brake", l.Brake), ("Rpm", l.Rpm), ("Speed",l.Speed), ("Steering",l.Steering), ("Throttle", l.Throttle)))
      .transpose
      .map(s => Series(s.head._1, s.map(l => l._2)))
  }
}

object Chart {
  def saveToLocal(series: Series, fileName: String) = {
    val xData = (0 until series.points.toList.length).map(_.toDouble).toArray
    val yData = series.points.toArray
    val chart = QuickChart.getChart("Log of Project Cars 2", "X", "Y", series.title, xData, yData)
    VectorGraphicsEncoder.saveVectorGraphic(chart, fileName, VectorGraphicsFormat.SVG)
  }
}
