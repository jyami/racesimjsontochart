package net.jyami.lambda

import java.io._

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.amazonaws.services.lambda.runtime.events.S3Event
import net.jyami.lambda.aws.AwsS3
import net.jyami.lambda.chart.{Chart, ChartSource, FileName, Series}


class PC2LoggerCreateGraphService extends RequestHandler[S3Event, String] {

  def getSeriesFromObject(bucket: String, key: String): Seq[Series] = {
    val obj = AwsS3.getObject(bucket, key)
    val json = AwsS3.getTextFromStream(obj)
    ChartSource.fromJson(json)
  }

  def handleRequest(event: S3Event, context: Context): String = {

    for {
      (srcBucket, srcKey) <- AwsS3.getSource(event)
      series <- getSeriesFromObject(srcBucket, srcKey)
    } yield {
      val fileName = FileName.generate()
      Chart.saveToLocal(series, fileName)

      val key = FileName.toKey(fileName)
      AwsS3.putObject(srcBucket, key, new File(fileName))
    }

    "Success"
  }
}
