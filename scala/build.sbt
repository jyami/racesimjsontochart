val Version = "0.1-SNAPSHOT"

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "net.jyami",
      scalaVersion := "2.10.7"
    )),
    name := "SampleLambda",
    libraryDependencies ++= Seq(
      "com.amazonaws" % "aws-lambda-java-core" % "1.2.0",
      "com.amazonaws" % "aws-lambda-java-events" % "2.2.4",
      "com.amazonaws" % "aws-java-sdk-s3" % "1.11.463",
      "org.json4s" %% "json4s-native" % "3.6.2",
      "org.knowm.xchart" % "xchart" % "3.5.2"

    ),
    assemblyJarName in assembly := "sample-lambda-%s.jar" format(Version)
  )
assemblyMergeStrategy in assembly := {
  case PathList(ps @ _*) if ps.last endsWith ".properties" => MergeStrategy.first
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}